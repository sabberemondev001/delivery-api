require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const fs = require("fs");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const swaggerUi = require("swagger-ui-express");
const YAML = require("yamljs");
const expressSession = require("express-session");
const MongoDBStore = require("connect-mongo")(expressSession);
const http = require("http");
const app = express();
const httpServer = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(httpServer);
// const corsOptions = require("./config/cors.config");
const swaggerUsersDocs = YAML.load("./docs/swagger/users.yaml");
const swaggerRestaurantsDocs = YAML.load("./docs/swagger/restaurants.yaml");
const generalDocs = YAML.load("./docs/swagger/general.yaml");
const authUserRoute = require("./routes/users/auth.user.route");
const authRestaurantRoute = require("./routes/restaurants/auth.restaurant.route");
const attributesRoute = require("./routes/attributes.route");
const categoriesRoute = require("./routes/categories.route");
const couponsRoute = require("./routes/coupons.route");
const restaurantsRoute = require("./routes/restaurants/index.restaurant.route");
const usersRoute = require("./routes/users/index.user.route");

// cors
app.use(cors());
// logging middleware
app.use(morgan("dev", {}));

// log only 5xx responses to log file
app.use(
  morgan("common", {
    stream: fs.createWriteStream(path.join(__dirname, "error.log"), {
      flags: "a",
    }),
    skip: function (req, res) {
      return res.statusCode < 500;
    },
  })
);

app.use(bodyParser.json({ limit: "100000mb" }));
app.use(bodyParser.urlencoded({ limit: "100000mb", extended: true }));

// session store
const sessionStore = new MongoDBStore({
  mongooseConnection: mongoose.connection,
  collection: "sessions",
});
// session config
app.use(
  expressSession({
    secret: process.env.SESSION_SECRET,
    store: sessionStore,
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 1000 * 60 * 60 * 24,
    },
  })
);

// database connection
let databaseConnected = false;

// routes
app.get("/", (req, res) => {
  res.json({
    success: true,
    message: "Welcome to the delivery api",
    databaseConnected,
    errorLog: fs.existsSync(path.join(__dirname, "error.log")),
    errorLogUrl: fs.existsSync(path.join(__dirname, "error.log"))
      ? `${process.env.BASE_URL}/error-log`
      : null,
  });
});
app.get("/error-log", (req, res) => {
  if (!fs.existsSync(path.join(__dirname, "error.log"))) {
    return res.status(404).json({
      success: false,
      message: "No error log file found",
    });
  }
  res.sendFile(path.join(__dirname, "error.log"));
});
app.use("/docs", express.static(path.join(__dirname, "./index.html")));
app.use(
  "/swagger-docs/users",
  swaggerUi.serveFiles(swaggerUsersDocs, {}),
  swaggerUi.setup(swaggerUsersDocs)
);
app.use(
  "/swagger-docs/restaurants",
  swaggerUi.serveFiles(swaggerRestaurantsDocs, {}),
  swaggerUi.setup(swaggerRestaurantsDocs)
);
app.use(
  "/swagger-docs/general",
  swaggerUi.serveFiles(generalDocs, {}),
  swaggerUi.setup(generalDocs)
);

// users routes
app.use("/api/users/auth", authUserRoute);
app.use("/api/users", usersRoute);

// restaurants routes
app.use("/api/restaurants/auth", authRestaurantRoute);
app.use("/api/restaurants", restaurantsRoute);

// attributes , categories & coupons routes
app.use("/api/attributes", attributesRoute);
app.use("/api/categories", categoriesRoute);
app.use("/api/coupons", couponsRoute);

// test socket for orders
io.on("connection", (socket) => {
  socket.on("place-order", (id) => {
    socket.join(id);

    // order will be tracked live using this private room for each order
  });
});

const connectWithRetry = () => {
  mongoose.set("strictQuery", true);
  return mongoose
    .connect(process.env.MONGODB_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(() => {
      databaseConnected = true;
      console.log("MongoDB is connected");
    })
    .catch((err) => {
      console.log(
        "MongoDB connection unsuccessful, retry after 5 seconds.",
        err
      );
      setTimeout(connectWithRetry, 5000);
    });
};

connectWithRetry();

httpServer.listen(process.env.PORT, () => {
  console.log(`Server is running on port http://localhost:${process.env.PORT}`);
  console.log(
    `Swagger docs part one is available at http://localhost:${process.env.PORT}/swagger-docs/restaurants/`
  );
  console.log(
    `Swagger docs part two is available at http://localhost:${process.env.PORT}/swagger-docs/users/`
  );
  console.log(
    `Swagger docs part three is available at http://localhost:${process.env.PORT}/swagger-docs/general/`
  );
  console.log(
    `Io tests are available at http://localhost:${process.env.PORT}/docs and source code is available at "index.html"`
  );
});
