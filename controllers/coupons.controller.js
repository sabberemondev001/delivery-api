const CouponModel = require("../models/Coupon.model");

// create a new coupon
exports.createCoupon = async (req, res) => {
  const {
    code,
    discountType,
    discountValue,
    expiryDate,
    applicableRestaurants,
    description,
    imgUrl,
  } = req.body;

  if (!code || !discountType || !discountValue || !expiryDate) {
    return res.status(400).json({
      success: false,
      message: "Code, discountType, discountValue, expiryDate  are required",
    });
  }

  if (discountType !== "percentage" && discountType !== "fixed") {
    return res.status(400).json({
      success: false,
      message: "DiscountType can only be percentage or fixed",
    });
  }

  if (
    discountType === "percentage" &&
    (discountValue < 0 || discountValue > 100)
  ) {
    return res.status(400).json({
      success: false,
      message: "DiscountValue must be between 0 and 100",
    });
  }

  try {
    const newCoupon = await CouponModel.create({
      code,
      discountType,
      discountValue,
      expiryDate,
      applicableRestaurants,
      description,
      imgUrl,
    });

    if (!newCoupon) {
      return res.status(500).json({
        success: false,
        message: "Error creating coupon",
      });
    }
    return res.status(201).json({
      success: true,
      message: "Coupon created",
      coupon: newCoupon,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      message: "Error creating coupon",
      error,
    });
  }
};

// get a coupon by coupon code
exports.getCouponByCode = async (req, res) => {
  const { code } = req.params;

  if (!code) {
    return res.status(400).json({
      success: false,
      message: "Code is required",
    });
  }

  try {
    // find coupon by code
    const coupon = await CouponModel.findOne({ code });

    if (!coupon) {
      return res.status(404).json({
        success: false,
        message: "Coupon not found",
      });
    }

    return res.status(200).json({
      success: true,
      message: "Coupon found",
      coupon,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      message: "Error getting coupon",
      error,
    });
  }
};

// delete a coupon by coupon code
exports.deleteCouponByCode = async (req, res) => {
  const { code } = req.params;

  if (!code) {
    return res.status(400).json({
      success: false,
      message: "Code is required",
    });
  }

  try {
    // find coupon by code
    const coupon = await CouponModel.findOne({ code });

    if (!coupon) {
      return res.status(404).json({
        success: false,
        message: "Coupon not found",
      });
    }

    // delete coupon
    await coupon.delete();

    return res.status(200).json({
      success: true,
      message: "Coupon deleted",
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      message: "Error deleting coupon",
      error,
    });
  }
};

// disable a coupon by coupon code
exports.disableCouponByCode = async (req, res) => {
  const { code } = req.params;

  if (!code) {
    return res.status(400).json({
      success: false,
      message: "Code is required",
    });
  }

  try {
    // find coupon by code
    const coupon = await CouponModel.findOne({ code });

    if (!coupon) {
      return res.status(404).json({
        success: false,
        message: "Coupon not found",
      });
    }

    // check if coupon is already disabled
    if (!coupon.enabled) {
      return res.status(400).json({
        success: false,
        message: "Coupon already disabled",
      });
    }

    // disable coupon
    coupon.enabled = false;
    await coupon.save();

    return res.status(200).json({
      success: true,
      message: "Coupon disabled",
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      message: "Error disabling coupon",
      error,
    });
  }
};
