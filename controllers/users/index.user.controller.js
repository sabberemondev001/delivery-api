const OrderModel = require("../../models/users/Order.model");

exports.addToCart = async (req, res) => {
  if (!req.session.cart) {
    req.session.cart = {
      items: [],
      restaurant: undefined,
      totalQuantity: 0,
      totalPrice: 0,
    };
  }

  const { productDetails, quantity, restaurant } = req.body;

  const cart = req.session.cart;

  const itemIndex = cart.items.findIndex(
    (item) => item.productDetails._id === productDetails._id
  );

  if (itemIndex > -1) {
    cart.items[itemIndex].quantity += quantity;
    cart.items[itemIndex].totalPrice =
      cart.items[itemIndex].quantity *
      cart.items[itemIndex].productDetails.price;
  } else {
    cart.items.push({
      productDetails,
      quantity,
      totalPrice: quantity * productDetails.price,
    });
  }

  cart.totalQuantity += quantity;
  cart.totalPrice += quantity * productDetails.price;
  cart.restaurant = restaurant;

  res.status(200).json({
    success: true,
    message: "Item added to cart",
    cart,
  });
};

exports.removeFromCart = async (req, res) => {
  const { productId } = req.params;

  const cart = req.session.cart;

  const itemIndex = cart.items.findIndex(
    (item) => item.productDetails._id === productId
  );

  if (itemIndex > -1) {
    cart.totalQuantity -= cart.items[itemIndex].quantity;
    cart.totalPrice -= cart.items[itemIndex].totalPrice;
    cart.items.splice(itemIndex, 1);
  }

  res.status(200).json({
    success: true,
    message: "Item removed from cart",
    cart,
  });
};

exports.getCartDetails = async (req, res) => {
  const cart = req.session.cart;

  if (!cart) {
    return res.status(200).json({
      success: true,
      cart: {
        items: [],
        totalQuantity: 0,
        totalPrice: 0,
      },
    });
  }

  res.status(200).json({
    success: true,
    cart,
  });
};

exports.placeOrder = async (req, res) => {
  const { deliveryAddress, type } = req.body;

  const cart = req.session.cart;

  if (!cart) {
    return res.status(400).json({
      success: false,
      message: "Cart is empty",
    });
  }

  const order = new OrderModel({
    // client: req.user._id,
    // restaurant: cart.restaurant,
    totalAmount: cart.totalPrice,
    type: type ? type : "delivery",
    deliveryAddress,
    items: cart.items,
  });

  await order.save();

  req.session.cart = {
    items: [],
    restaurant: undefined,
    totalQuantity: 0,
    totalPrice: 0,
  };

  res.status(200).json({
    success: true,
    message: "Order placed successfully",
  });
};
