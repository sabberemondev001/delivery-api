const AttributeModel = require("../models/Attribute.model");

// create a new attribute
exports.createAttribute = async (req, res) => {
  const { name, type } = req.body;

  if (!name || !type) {
    return res.status(400).json({
      success: false,
      message: "Name and type(item/review) are required",
    });
  }

  if (type !== "item" && type !== "review") {
    return res.status(400).json({
      success: false,
      message: "Type can only be item or review",
    });
  }

  const newAttribute = await AttributeModel.create({ name, type });

  if (!newAttribute) {
    return res.status(500).json({
      success: false,
      message: "Error creating attribute",
    });
  }

  return res.status(201).json({
    success: true,
    message: "Attribute created",
    attribute: newAttribute,
  });
};

// get all attributes filtered by type
exports.getAllAttributes = async (req, res) => {
  const { type } = req.query;

  if (!type) {
    return res.status(400).json({
      success: false,
      message: "Type(item/review) is required",
    });
  }

  if (type !== "item" && type !== "review" && type !== "all") {
    return res.status(400).json({
      success: false,
      message: "Type can only be item or review or all",
    });
  }

  if (type === "all") {
    const attributes = await AttributeModel.find();

    if (!attributes) {
      return res.status(500).json({
        success: false,
        message: "Error getting attributes",
      });
    }

    return res.status(200).json({
      success: true,
      message: "Attributes retrieved",
      attributes,
    });
  }

  const attributes = await AttributeModel.find({ type });

  if (!attributes) {
    return res.status(500).json({
      success: false,
      message: "Error getting attributes",
    });
  }

  return res.status(200).json({
    success: true,
    message: "Attributes retrieved",
    attributes,
  });
};

// delete an attribute
exports.deleteAttribute = async (req, res) => {
  const { attributeId } = req.params;

  if (!attributeId) {
    return res.status(400).json({
      success: false,
      message: "Attribute id is required",
    });
  }

  // check if attribute exists
  const attributeExists = await AttributeModel.findById(attributeId);

  if (!attributeExists) {
    return res.status(404).json({
      success: false,
      message: "Attribute not found",
    });
  }

  const deletedAttribute = await AttributeModel.findByIdAndDelete(attributeId);

  if (!deletedAttribute) {
    return res.status(500).json({
      success: false,
      message: "Error deleting attribute",
    });
  }

  return res.status(200).json({
    success: true,
    message: "Attribute deleted",
  });
};
