const mongoose = require("mongoose");
const CategoryModel = require("../models/Category.model");

// create a new category
exports.createCategory = async (req, res) => {
  const { name, description, imgUrl, reviewAttributes } = req.body;

  if (!name) {
    return res.status(400).json({
      success: false,
      message: "Name is required",
    });
  }

  const newCategory = await CategoryModel.create({
    name,
    description,
    imgUrl,
    reviewAttributes,
  });

  if (!newCategory) {
    return res.status(500).json({
      success: false,
      message: "Error creating category",
    });
  }

  return res.status(201).json({
    success: true,
    message: "Category created",
    category: newCategory,
  });
};

// get all categories
exports.getAllCategories = async (req, res) => {
  const categories = await CategoryModel.find();

  if (!categories) {
    return res.status(500).json({
      success: false,
      message: "Error getting categories",
    });
  }

  return res.status(200).json({
    success: true,
    message: "Categories retrieved",
    categories,
  });
};

// get a category by category id
exports.getCategoryById = async (req, res) => {
  const { categoryId } = req.params;

  if (!mongoose.Types.ObjectId.isValid(categoryId)) {
    return res.status(400).json({
      success: false,
      message: "Category id is invalid",
    });
  }

  const category = await CategoryModel.findById(categoryId);

  if (!category) {
    return res.status(404).json({
      success: false,
      message: "Category not found",
    });
  }

  return res.status(200).json({
    success: true,
    message: "Category retrieved",
    category,
  });
};

// delete a category by category id
exports.deleteCategoryById = async (req, res) => {
  const { categoryId } = req.params;

  if (!categoryId || !mongoose.Types.ObjectId.isValid(categoryId)) {
    return res.status(400).json({
      success: false,
      message: "Category id is required",
    });
  }

  const category = await CategoryModel.findById(categoryId);

  if (!category) {
    return res.status(404).json({
      success: false,
      message: "Category not found",
    });
  }

  const deletedCategory = await CategoryModel.findByIdAndDelete(categoryId);

  if (!deletedCategory) {
    return res.status(500).json({
      success: false,
      message: "Error deleting category",
    });
  }

  return res.status(200).json({
    success: true,
    message: "Category deleted",
  });
};

// search category by name
exports.searchCategoryByName = async (req, res) => {
  const { name } = req.query;

  if (!name) {
    return res.status(400).json({
      success: false,
      message: "Name(partial/full) is required",
    });
  }

  const categories = await CategoryModel.find({
    name: { $regex: name, $options: "i" },
  });

  if (!categories) {
    return res.status(500).json({
      success: false,
      message: "Error searching category",
    });
  }

  return res.status(200).json({
    success: true,
    message: "Search category success",
    categories,
  });
};
