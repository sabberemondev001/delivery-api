const RestaurantModel = require("../../models/restaurants/Restaurant.model");
const { v4: uuidv4 } = require("uuid");

// add a new restaurant
exports.addRestaurant = async (req, res) => {
  const {
    name,
    description,
    imgUrl,
    gallery,
    category,
    mobile,
    address,
    services,
    timing,
    dineInEnabled,
    dineInSettings,
    deliveryCharges,
    longitude,
    latitude,
    restaurantAdminUuid,
  } = req.body;

  if (
    !name ||
    !mobile ||
    !address ||
    !timing ||
    !category ||
    !deliveryCharges ||
    !restaurantAdminUuid ||
    !longitude ||
    !latitude
  ) {
    return res.status(400).json({
      success: false,
      message: "Please provide all required fields",
    });
  }

  const data = {
    name,
    mobile,
    address,
    timing,
    category,
    deliveryCharges,
    uuid: uuidv4(),
    restaurantAdminUuid,
    longitude,
    latitude,
  };

  if (description) data.description = description;
  if (imgUrl) data.imgUrl = imgUrl;
  if (gallery) data.gallery = gallery;
  if (services) data.services = services;
  if (dineInEnabled) data.dineInEnabled = dineInEnabled;
  if (dineInSettings) data.dineInSettings = dineInSettings;

  try {
    const restaurant = await RestaurantModel.create(data);

    if (!restaurant)
      return res.status(500).json({
        success: false,
        message: "Error adding restaurant",
      });

    return res.status(201).json({
      success: true,
      message: "Restaurant added successfully",
      restaurant,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      message: "Server error",
      error,
    });
  }
};

// get all restaurants with pagination
exports.getAllRestaurants = async (req, res) => {
  const { page, limit } = req.query;

  if (!page || !limit) {
    return res.status(400).json({
      success: false,
      message: "Please provide page and limit",
    });
  }

  try {
    // use the static paginate method to get all the restaurants with pagination
    const { restaurants, totalPages, currentPage } =
      await RestaurantModel.paginate(page, limit);

    if (!restaurants)
      return res.status(500).json({
        success: false,
        message: "Error getting restaurants",
      });

    return res.status(200).json({
      success: true,
      message: "Restaurants fetched successfully",
      restaurants,
      totalPages,
      currentPage,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      message: "Server error",
      error,
    });
  }
};

// get restaurant by uuid
exports.getRestaurantByUuid = async (req, res) => {
  const { uuid } = req.params;

  if (!uuid) {
    return res.status(400).json({
      success: false,
      message: "Please provide uuid",
    });
  }

  try {
    // get a restaurant by uuid and populate category
    const restaurant = await RestaurantModel.findOne({ uuid }).populate(
      "category"
    );

    if (!restaurant) {
      return res.status(404).json({
        success: false,
        message: "Restaurant not found",
      });
    }

    return res.status(200).json({
      success: true,
      message: "Restaurant fetched successfully",
      restaurant,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      message: "Server error",
      error,
    });
  }
};

// get restearants of a restaurant admin by uuid
exports.getRestaurantsByRestaurantAdminUuid = async (req, res) => {
  const { restaurantAdminUuid } = req.params;

  if (!restaurantAdminUuid) {
    return res.status(400).json({
      success: false,
      message: "Please provide restaurant admin uuid",
    });
  }

  try {
    // get all restaurants of a restaurant admin by uuid and populate category
    const restaurants = await RestaurantModel.find({
      restaurantAdminUuid,
    }).populate("category");

    return res.status(200).json({
      success: true,
      message: "Restaurants fetched successfully",
      restaurants,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      message: "Server error",
      error,
    });
  }
};

exports.deleteRestaurant = async (req, res) => {
  const { uuid } = req.params;

  if (!uuid) {
    return res.status(400).json({
      success: false,
      message: "Please provide uuid",
    });
  }

  try {
    // check if restaurant exists
    const restaurant = await RestaurantModel.findOne({ uuid });

    if (!restaurant) {
      return res.status(404).json({
        success: false,
        message: "Restaurant not found",
      });
    }

    // delete restaurant
    await RestaurantModel.deleteOne({ uuid });

    return res.status(200).json({
      success: true,
      message: "Restaurant deleted successfully",
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      message: "Server error",
      error,
    });
  }
};
