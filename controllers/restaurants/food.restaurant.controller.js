const FoodModel = require("../../models/restaurants/Food.model");

// add food
exports.addFood = async (req, res) => {
  const {
    name,
    price,
    discountedPrice,
    restaurant,
    category,
    quantity,
    imgUrl,
    description,
    publish,
    nonVeg,
    takewayOption,
    ingredientProperties,
    addOns,
  } = req.body;

  if (!name || !price || !restaurant) {
    return res
      .status(400)
      .json({ success: false, message: "All fields are required" });
  }

  try {
    const food = new FoodModel({
      name,
      price,
      discountedPrice,
      restaurant,
      category,
      quantity,
      imgUrl,
      description,
      publish,
      nonVeg,
      takewayOption,
      ingredientProperties,
      addOns,
    });

    await food.save();

    res.status(201).json({
      success: true,
      message: "Food added successfully",
      food,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

// get all foods
exports.getAllFoods = async (req, res) => {
  const { page, limit } = req.query;

  if (!page || !limit) {
    return res
      .status(400)
      .json({ success: false, message: "Page and limit are required" });
  }

  try {
    const { foods, totalPages, currentPage } = await FoodModel.paginate(
      page,
      limit
    );

    res.status(200).json({
      success: true,
      message: "All foods",
      foods,
      totalPages,
      currentPage,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

// get food by id
exports.getFoodById = async (req, res) => {
  const { id } = req.params;

  try {
    const food = await FoodModel.findById(id);

    if (!food) {
      return res
        .status(404)
        .json({ success: false, message: "Food not found" });
    }

    res.status(200).json({ success: true, message: "Food found", food });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

// delete food by id
exports.deleteFoodById = async (req, res) => {
  const { id } = req.params;

  try {
    // check if food exists
    const food = await FoodModel.findById(id);

    if (!food) {
      return res
        .status(404)
        .json({ success: false, message: "Food not found" });
    }

    await food.remove();

    res.status(200).json({ success: true, message: "Food deleted" });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};
