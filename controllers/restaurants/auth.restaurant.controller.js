const RestaurantAdminModel = require("../../models/restaurants/RestaurantAdmin.model");
const OTPModel = require("../../models/OTP.model");
const { sendMail } = require("../../utils/sendMail.util");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { v4: uuidv4 } = require("uuid");

// register a new user
exports.registerUser = async (req, res) => {
  const { email, password, firstName, lastName, profilePicUrl, mobile } =
    req.body;

  if (!email || !password) {
    return res.status(400).json({
      success: false,
      message: "Email and password are required",
    });
  }

  // check if user already exists
  const user = await RestaurantAdminModel.findOne({ email });
  if (user) {
    return res.status(403).json({
      success: false,
      message: "User already exists",
    });
  }

  // send verification email with OTP
  const otp = Math.floor(100000 + Math.random() * 900000);
  const hashedOTP = await bcrypt.hash(otp.toString(), 10);
  const otpExpires = Date.now() + 10 * 60 * 1000; // 10 minutes

  const mailOptions = {
    email,
    subject: "Verify your email",
    message: `<h1>OTP: ${otp}</h1>`,
  };

  const mailResponse = await sendMail(mailOptions);
  if (!mailResponse.success) {
    return res.status(500).json({
      success: false,
      message: "Error sending OTP",
    });
  }

  const newOTP = new OTPModel({
    OTP: hashedOTP,
    uuid: uuidv4(),
    expiresAt: otpExpires,
  });

  const savedOTP = await newOTP.save();

  // hash password
  const hashedPassword = await bcrypt.hash(password, 10);

  // create new user
  const newUser = new RestaurantAdminModel({
    email,
    password: hashedPassword,
    firstName,
    lastName,
    profilePicUrl,
    mobile,
    uuid: uuidv4(),
  });

  const savedUser = await newUser.save();

  if (!savedUser) {
    return res.status(500).json({
      success: false,
      message: "Error creating user",
    });
  }

  return res.status(201).json({
    success: true,
    message: "User created. OTP sent to email",
    OTPID: savedOTP.uuid,
    newUser: {
      email: savedUser.email,
      uuid: savedUser.uuid,
    },
  });
};

// verify user email
exports.verifyEmail = async (req, res) => {
  const { OTPID, OTP, uuid } = req.body;

  if (!OTPID || !OTP || !uuid) {
    return res.status(400).json({
      success: false,
      message: "OTPID, OTP and user uuid are required",
    });
  }

  // find OTP
  const otp = await OTPModel.findOne({ uuid: OTPID });
  if (!otp) {
    return res.status(404).json({
      success: false,
      message: "Invalid OTPID",
    });
  }

  // check if OTP is expired
  if (otp.expiresAt < Date.now()) {
    return res.status(403).json({
      success: false,
      message: "OTP expired",
    });
  }

  // verify OTP
  const isOTPValid = await bcrypt.compare(OTP, otp.OTP);
  if (!isOTPValid) {
    return res.status(403).json({
      success: false,
      message: "Invalid OTP",
    });
  }

  // find user
  const user = await RestaurantAdminModel.findOne({ uuid });
  if (!user) {
    return res.status(404).json({
      success: false,
      message: "Invalid user uuid",
    });
  }

  // update user
  const updatedUser = await RestaurantAdminModel.findOneAndUpdate(
    { uuid },
    { emailVerified: true },
    { new: true }
  );

  if (!updatedUser) {
    return res.status(500).json({
      success: false,
      message: "Error updating user",
    });
  }

  // create jwt token
  const token = jwt.sign(
    {
      uuid: updatedUser.uuid,
      email: updatedUser.email,
    },
    process.env.JWT_SECRET,
    {
      expiresIn: "7d",
    }
  );

  return res.status(200).json({
    success: true,
    message: "Email verified",
    user: {
      email: updatedUser.email,
      uuid: updatedUser.uuid,
    },
    token,
  });
};

// login user
exports.loginUser = async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(400).json({
      success: false,
      message: "Email and password are required",
    });
  }

  // find user
  const user = await RestaurantAdminModel.findOne({ email });
  if (!user) {
    return res.status(404).json({
      success: false,
      message: "Invalid email",
    });
  }

  // check if email is verified
  if (!user.emailVerified) {
    return res.status(403).json({
      success: false,
      message: "Email not verified",
    });
  }

  // check if the password is correct
  const isPasswordValid = await bcrypt.compare(password, user.password);
  if (!isPasswordValid) {
    return res.status(403).json({
      success: false,
      message: "Invalid password",
    });
  }

  // create jwt token
  const token = jwt.sign(
    {
      uuid: user.uuid,
      email: user.email,
    },
    process.env.JWT_SECRET,
    {
      expiresIn: "7d",
    }
  );

  return res.status(200).json({
    success: true,
    message: "User logged in",
    user: {
      email: user.email,
      uuid: user.uuid,
      firstName: user.firstName,
      lastName: user.lastName,
      profilePicUrl: user.profilePicUrl,
      mobile: user.mobile,
    },
    token,
  });
};
