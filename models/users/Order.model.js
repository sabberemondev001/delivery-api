const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
  {
    client: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      //   required: true,
    },
    restaurant: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Restaurant",
      //   required: true,
    },
    // driver: {},
    totalAmount: {
      type: Number,
      required: true,
    },
    type: {
      type: String,
      default: "delivery", // possible values: delivery, takeAway
    },
    status: {
      type: String,
      default: "placed", // possible values:  accepted, rejected, completed, cancelled, placed
    },
    deliveryAddress: {
      type: {
        latitude: Number,
        longitude: Number,
        address: String,
      },
    },
    items: {
      type: [
        {
          productDetails: Object,
          quantity: Number,
        },
      ],
    },
    clientReview: {
      type: {
        rating: Number,
        comment: String,
      },
    },
  },
  { timestamps: true, versionKey: false }
);

module.exports = mongoose.model("Order", orderSchema);
