const mongoose = require("mongoose");

const couponSchema = new mongoose.Schema(
  {
    code: {
      type: String,
      required: true,
      unique: true,
    },
    discountType: {
      type: String,
      required: true, // possible values: "percentage", "fixed"
    },
    discountValue: {
      type: Number,
      required: true, // if discountType is "percentage", then discountValue is a number between 0 and 100
    },
    expiryDate: {
      type: Date,
      required: true,
    },
    // reference to the restaurants that this coupon is applicable to
    applicableRestaurants: {
      type: [mongoose.Schema.Types.ObjectId],
      ref: "Restaurant",
      default: [], // if empty, then this coupon is applicable to all restaurants
    },
    description: {
      type: String,
    },
    imgUrl: {
      type: String,
    },
    enabled: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true, versionKey: false }
);

module.exports = mongoose.model("Coupon", couponSchema);
