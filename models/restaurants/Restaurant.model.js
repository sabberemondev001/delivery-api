const mongoose = require("mongoose");

const restaurantSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
    },
    imgUrl: {
      type: String,
    },
    gallery: {
      type: Array,
      default: [],
    },
    // ref of category to populate
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category",
      required: true,
    },
    mobile: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    services: {
      type: Array,
      default: [],
    },
    timing: {
      type: {
        open: String,
        close: String,
      },
      required: true,
    },
    dineInEnabled: {
      type: Boolean,
      default: false,
    },
    dineInSettings: {
      type: {
        openingTime: String,
        closingTime: String,
        cost: Number,
        menuCardImgUrls: Array,
      },
    },
    deliveryCharges: {
      type: {
        perKm: Number,
        minimum: Number,
        minimumWithinKm: Number,
      },
      required: true,
    },
    longitude: {
      type: Number,
      required: true,
    },
    latitude: {
      type: Number,
      required: true,
    },
    uuid: {
      type: String,
      required: true,
      unique: true,
    },
    restaurantAdminUuid: {
      type: String,
      required: true,
    },
  },
  { timestamps: true, versionKey: false }
);

// static paginate method to get all the restaurants with pagination( page and limit provided by user)
restaurantSchema.statics.paginate = async function (page, limit) {
  const restaurants = await this.find()
    .populate("category")
    .limit(Number(limit) * 1)
    .skip((Number(page) - 1) * Number(limit))
    .exec();

  const count = await this.countDocuments();

  return {
    restaurants,
    totalPages: Math.ceil(count / Number(limit)),
    currentPage: Number(page),
  };
};

module.exports = mongoose.model("Restaurant", restaurantSchema);
