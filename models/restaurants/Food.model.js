const mongoose = require("mongoose");

const foodSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    discountedPrice: {
      type: Number,
    },
    restaurant: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Restaurant",
      required: true,
    },
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category",
    },
    quantity: {
      type: Number,
      default: 1,
    },
    imgUrl: {
      type: String,
    },
    description: {
      type: String,
    },
    publish: {
      type: Boolean,
      default: false,
    },
    nonVeg: {
      type: Boolean,
      default: false,
    },
    takewayOption: {
      type: Boolean,
      default: false,
    },
    ingredientProperties: {
      type: {
        calories: Number,
        fats: Number,
        proteins: Number,
        grams: Number,
      },
    },
    addOns: {
      type: [
        {
          title: String,
          price: Number,
        },
      ],
    },
  },
  { timestamps: true, versionKey: false }
);

// static method paginate with page and limit
foodSchema.statics.paginate = async function (p, l) {
  const page = Number(p);
  const limit = Number(l);

  const foods = await this.find()
    .populate("restaurant", ["name", "uuid", "imgUrl"])
    .populate("category", ["name"])
    .limit(Number(limit) * 1)
    .skip((Number(page) - 1) * Number(limit))
    .exec();

  const count = await this.countDocuments();

  return {
    foods,
    totalPages: Math.ceil(count / Number(limit)),
    currentPage: Number(page),
  };
};

module.exports = mongoose.model("Food", foodSchema);
