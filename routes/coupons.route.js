const express = require("express");
const couponsController = require("../controllers/coupons.controller");

const router = express.Router();

// routes
router.post("/", couponsController.createCoupon);
router.get("/:code", couponsController.getCouponByCode);
router.delete("/:code", couponsController.deleteCouponByCode);
router.put("/disable/:code", couponsController.disableCouponByCode);

module.exports = router;
