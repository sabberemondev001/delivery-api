const express = require("express");
const categoriesController = require("../controllers/categories.controller");

const router = express.Router();

// routes
router.post("/", categoriesController.createCategory);
router.get("/", categoriesController.getAllCategories);
router.get("/:categoryId", categoriesController.getCategoryById);
router.delete("/:categoryId", categoriesController.deleteCategoryById);
router.get("/search", categoriesController.searchCategoryByName);

module.exports = router;
