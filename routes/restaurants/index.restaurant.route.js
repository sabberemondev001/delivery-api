const express = require("express");
const controller = require("../../controllers/restaurants/index.restaurant.controller");
const foodsController = require("../../controllers/restaurants/food.restaurant.controller");

const router = express.Router();

// routes
router.get("/", controller.getAllRestaurants);
router.get("/restaurant/:uuid", controller.getRestaurantByUuid);
router.delete("/restaurant/:uuid", controller.deleteRestaurant);
router.get(
  "/admin/:restaurantAdminUuid",
  controller.getRestaurantsByRestaurantAdminUuid
);
router.post("/", controller.addRestaurant);

// foods routes
router.get("/foods", foodsController.getAllFoods);
router.get("/foods/:id", foodsController.getFoodById);
router.delete("/foods/:id", foodsController.deleteFoodById);
router.post("/foods", foodsController.addFood);

module.exports = router;
