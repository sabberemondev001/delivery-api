const express = require("express");
const authController = require("../../controllers/restaurants/auth.restaurant.controller");
const router = express.Router();

// routes
router.post("/register", authController.registerUser);
router.post("/verify-email", authController.verifyEmail);
router.post("/login", authController.loginUser);

module.exports = router;
