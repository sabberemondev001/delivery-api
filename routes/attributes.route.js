const express = require("express");
const attributesController = require("../controllers/attributes.controller");

const router = express.Router();

// routes
router.post("/", attributesController.createAttribute);
router.get("/", attributesController.getAllAttributes);
router.delete("/:attributeId", attributesController.deleteAttribute);

module.exports = router;
