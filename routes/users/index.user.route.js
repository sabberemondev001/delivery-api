const express = require("express");
const controller = require("../../controllers/users/index.user.controller");

const router = express.Router();

router.get("/cart", controller.getCartDetails);
router.post("/cart", controller.addToCart);
router.delete("/cart/:productId", controller.removeFromCart);

router.post("/orders/place-order", controller.placeOrder);

module.exports = router;
