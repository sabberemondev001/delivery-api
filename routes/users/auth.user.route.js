const express = require("express");
const authController = require("../../controllers/users/auth.user.controller");
const router = express.Router();

// routes
router.post("/register", authController.registerUser);
router.post("/verify-email", authController.verifyEmail);
router.post("/login", authController.loginUser);

module.exports = router;
